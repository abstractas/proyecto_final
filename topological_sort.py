from collections import defaultdict 
'''
    Clase propuesta por Neelam Yadav, tomado de: https://www.geeksforgeeks.org/topological-sorting-indegree-based-solution/  
'''
class Graph: 
    '''
        Clase que contiene las caracteristicas de un DAG (Directed Acyclic Graph),
        asi como el algoritmo de ordenamiento topologico.
    '''
    def __init__(self, vertices): 
        '''
            Metodo que inicializa los parametros de la clase
            -Entradas:
                -vertices [int]: numero de vertices que contiene el grafo.
            -Salidas:
                -graph  [deafultdict]: diccionario que contiene las listas de adyacencias
                -V      [int]: instancia de la clase para el numero de verices 
        '''
        self.graph = defaultdict(list) 
        self.V = vertices 
  
    def addEdge(self, u, v): 
        '''
            Metodo que agrega nuevos bordes al grafo, hacendo una entrada al grafo relacionando 
            en forma de diccionario tal que: graph = {u : (v)}
                -Entradas:
                    -u [int]: borde
                    -v [int]: vertice
        '''
        self.graph[u].append(v) 

    def topologicalSort(self): 
        '''
            Metodo que implementa el algoritmo de ordenamiento topologico
        '''

        # Lista para almacenar el numero de bordes entrantes de todos los vertices, inicializada en 0.
        in_degree = [0]*(self.V) 

        # Iteracion para agregar los bordes entrantes de cada vertice por adyacencia en la lista de vertices 
        for i in self.graph: #itera sobre las llaves del diccionario
            for j in self.graph[i]: #itera sobre los valores de la llave actual
                in_degree[j] += 1
  

        # Lista para los vertices sin bordes entrantes (primeros elementos del sort)
        queue = [] 
        # Iteracion para anadir los vertices sin bordes entrantes a la lista queue
        for i in range(self.V): 
            if in_degree[i] == 0: 
                queue.append(i) 
  
        # Contador para los vertices verificados
        cnt = 0
  
        # Lista para anadir los vertices en el orden final
        top_order = [] 
  
        # Siempre que existan vertices sin bordes entrantes:
        while queue: 

            #Eliminar el primer vertice sin bordes entrantes de la lista y colocarlo en el ordenamiento final 
            u = queue.pop(0) 
            top_order.append(u) 
  
            # Iterar sobre todos los vertices adyacentes del vertice anadido a la lista final y disminuir sus vertices entrantes 
            for i in self.graph[u]: 
                in_degree[i] -= 1
                # Si no tiene vertices entrantes, se agrega al ordenamiento final 
                if in_degree[i] == 0: 
                    queue.append(i) 
                # Si sigue teniendo vertices entrantes, se repite la secuencia
            cnt += 1
  
        # Si la cantidad de vertices verificados es diferente a la cantidad de vertices introducidos, el grafico es ciclico
        if cnt != self.V: 
            print("Hay un ciclo en el grafo, ordenamiento no realizado")
        # Si la cantidad de vertices verificados es igual a los introducidos, se finalizar el ordenamiento.
        else :  
            print(top_order)

def prompt():
    '''
        Funcion que crea un grafo especificado por el usuario y crea las conexiones entre vertices segun indique el mismo
    '''

    # Input de la cantidad de vertices para el grafo
    print('\tIngrese la cantidad de vertices deseada:')
    V = int(input())

    # Especificaciones del metodo
    print('\n\tLas conexiones entre los vertices de la forma a->b se digitan como: a,b\n')
    print(f'\tEl vertice mas grande debe ser: {V-1}\n')

    # Creando una instancia de la clase
    g = Graph(V)

    # Iterando para agregar las conexiones al grafo
    for i in range(V):
        print(f'Ingrese la conexion entre vertices numero {i}: ')
        conection = str(input())
        conection = conection.split(',')
        u = int(conection[0])
        v = int(conection[1])
        g.addEdge(u,v)

    # Ejecutando ordenamieto
    print(f'El resultado del ordenamiento propuesto es:\n')
    g.topologicalSort() 

def default():
    '''
        Funcion que ejecuta la creacion del grafo con valores definidos y ejecuta el ordenamiento
    '''
    # Creando una instancia de la clase con 6 vertices
    print('Creando un grafo de 6 vertices...')
    g = Graph(6) 

    # Anadiendo bordes para cada uno de los vertices
    print('Anandiendo una conexion entre los vertices 5->2')
    g.addEdge(5,2) 
    print('Anandiendo una conexion entre los vertices 5->0')
    g.addEdge(5,0) 
    print('Anandiendo una conexion entre los vertices 4->0')
    g.addEdge(4,0) 
    print('Anandiendo una conexion entre los vertices 4->1')
    g.addEdge(4,1) 
    print('Anandiendo una conexion entre los vertices 2->3')
    g.addEdge(2,3) 
    print('Anandiendo una conexion entre los vertices 3->1')
    g.addEdge(3,1)

    print(f'El resultado del ordenamiento propuesto es:\n')
    # Ejecutando el ordenamiento
    g.topologicalSort()

        
def main():
    # Bienvenida
    print('\n********************\n* Topological Sort *\n********************')

    # Input del modo de operacion
    print('Seleccion del modo de operacion\n\tIngrese:\n\t\t[0] Grafo default\n\t\t[1] Seleccionar conexiones del grafo')
    mode = input()

    # Seleccion del modo de operacion
    if int(mode) == 0:
        default()
    elif int(mode) == 1:
        prompt()
    else:
        print('\nEntrada no valida, reiniciando programa...\n\n')
        main()

if __name__ == "__main__":
    main()
